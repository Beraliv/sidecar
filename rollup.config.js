import path from 'path';
import babel from 'rollup-plugin-babel';
import resolve from 'rollup-plugin-node-resolve';
import postcss from 'rollup-plugin-postcss';
import { uglify } from 'rollup-plugin-uglify';

import manifest from './package.json';
const getPostcssPluginStack = require('./postcss-plugin-stack');

export default {
  input: './src/index.js',
  output: {
    file: './dist/sidecar.rollup.js',
    library: 'sidecar',
    format: 'cjs'
  },
  plugins: [
    resolve(),
    babel({
      exclude: 'node_modules/**',
    }),
    postcss({
      plugins: getPostcssPluginStack(),
    }),
    uglify(),
  ]
};
