'use strict';

// via https://developer.mozilla.org/en-US/docs/Web/API/CustomEvent/CustomEvent#Polyfill
function CustomEvent(event, _ref) {
  var _ref$bubbles = _ref.bubbles,
      bubbles = _ref$bubbles === void 0 ? false : _ref$bubbles,
      _ref$cancelable = _ref.cancelable,
      cancelable = _ref$cancelable === void 0 ? false : _ref$cancelable,
      _ref$detail = _ref.detail,
      detail = _ref$detail === void 0 ? undefined : _ref$detail;
  var evt;

  try {
    evt = new window.CustomEvent(event, {
      bubbles: bubbles,
      cancelable: cancelable,
      detail: detail
    });
  } catch (e) {
    // For IE11-
    evt = document.createEvent('CustomEvent');
    evt.initCustomEvent(event, bubbles, cancelable, detail);
  }

  return evt;
}
CustomEvent.prototype = window.Event.prototype;

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

function _defineProperties(target, props) {
  for (var i = 0; i < props.length; i++) {
    var descriptor = props[i];
    descriptor.enumerable = descriptor.enumerable || false;
    descriptor.configurable = true;
    if ("value" in descriptor) descriptor.writable = true;
    Object.defineProperty(target, descriptor.key, descriptor);
  }
}

function _createClass(Constructor, protoProps, staticProps) {
  if (protoProps) _defineProperties(Constructor.prototype, protoProps);
  if (staticProps) _defineProperties(Constructor, staticProps);
  return Constructor;
}

// We return the native Symbol if available
// Otherwise just give them a random string
function BasicSymbol(description) {
  if (typeof Symbol === 'function') {
    return Symbol(description);
  }

  var randomString = Math.random().toString(36).substr(2, 8);
  return description + randomString;
}

var remove = function remove(element) {
  if (element) {
    element.parentElement.removeChild(element);
  }
};

var ElementStore =
/*#__PURE__*/
function () {
  function ElementStore() {
    _classCallCheck(this, ElementStore);

    this.elements = [];
  }

  _createClass(ElementStore, [{
    key: "createElement",
    value: function createElement() {
      for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
        args[_key] = arguments[_key];
      }

      var element = document.createElement.apply(document, args);
      this.add(element);
      return element;
    }
  }, {
    key: "add",
    value: function add() {
      for (var _len2 = arguments.length, elements = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
        elements[_key2] = arguments[_key2];
      }

      var flattenedElements = [].concat(elements).reduce(function (prevResult, element) {
        if (element) {
          // If it is a store, flatten it out into us
          if (element instanceof ElementStore) {
            return prevResult.concat(element.elements);
          }

          return prevResult.concat(element);
        }

        return prevResult;
      }, []);
      this.elements = this.elements.concat(flattenedElements);
    }
  }, {
    key: "destroy",
    value: function destroy() {
      this.elements.forEach(function (element) {
        return remove(element);
      });
      this.elements = [];
    }
  }]);

  return ElementStore;
}();

function styleInject(css, ref) {
  if ( ref === void 0 ) ref = {};
  var insertAt = ref.insertAt;

  if (!css || typeof document === 'undefined') { return; }

  var head = document.head || document.getElementsByTagName('head')[0];
  var style = document.createElement('style');
  style.type = 'text/css';

  if (insertAt === 'top') {
    if (head.firstChild) {
      head.insertBefore(style, head.firstChild);
    } else {
      head.appendChild(style);
    }
  } else {
    head.appendChild(style);
  }

  if (style.styleSheet) {
    style.styleSheet.cssText = css;
  } else {
    style.appendChild(document.createTextNode(css));
  }
}

var css = ".gitter-hidden{box-sizing:border-box;display:none}.gitter-icon{box-sizing:border-box;width:22px;height:22px;fill:currentColor}.gitter-chat-embed{box-sizing:border-box;z-index:100;position:fixed;top:0;left:60%;bottom:0;right:0;display:-ms-flexbox;display:flex;-ms-flex-direction:row;flex-direction:row;background-color:#fff;border-left:1px solid #333;box-shadow:-12px 0 18px 0 rgba(50,50,50,.3);transition:transform .3s cubic-bezier(.16,.22,.22,1.7)}.gitter-chat-embed{box-sizing:border-box;background-color:#fff}.gitter-chat-embed.is-collapsed:not(.is-loading){box-sizing:border-box;transform:translateX(110%)}.gitter-chat-embed:after{box-sizing:border-box;content:'';z-index:-1;position:absolute;top:0;left:100%;bottom:0;right:-100%;background-color:#fff}.gitter-chat-embed:after{box-sizing:border-box;background-color:#fff}@media(max-width:1150px){.gitter-chat-embed{box-sizing:border-box;left:45%}}@media(max-width:944px){.gitter-chat-embed{box-sizing:border-box;left:30%}}@media(max-width:600px){.gitter-chat-embed{box-sizing:border-box;left:15%}}@media(max-width:500px){.gitter-chat-embed{box-sizing:border-box;left:0;border-left:none}}.gitter-chat-embed>iframe{box-sizing:border-box;-ms-flex:1;flex:1;width:100%;height:100%;border:0}.gitter-chat-embed-loading-wrapper{box-sizing:border-box;position:absolute;top:0;left:0;bottom:0;right:0;display:none;-ms-flex-pack:center;justify-content:center;-ms-flex-align:center;align-items:center}.is-loading .gitter-chat-embed-loading-wrapper{box-sizing:border-box;display:-ms-flexbox;display:flex}.gitter-chat-embed-loading-indicator{box-sizing:border-box;opacity:.75;background-image:url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxNzkyIDE3OTIiIGZpbGw9IiMzYTMxMzMiPjxwYXRoIGQ9Ik01MjYgMTM5NHEwIDUzLTM3LjUgOTAuNXQtOTAuNSAzNy41cS01MiAwLTkwLTM4dC0zOC05MHEwLTUzIDM3LjUtOTAuNXQ5MC41LTM3LjUgOTAuNSAzNy41IDM3LjUgOTAuNXptNDk4IDIwNnEwIDUzLTM3LjUgOTAuNXQtOTAuNSAzNy41LTkwLjUtMzcuNS0zNy41LTkwLjUgMzcuNS05MC41IDkwLjUtMzcuNSA5MC41IDM3LjUgMzcuNSA5MC41em0tNzA0LTcwNHEwIDUzLTM3LjUgOTAuNXQtOTAuNSAzNy41LTkwLjUtMzcuNS0zNy41LTkwLjUgMzcuNS05MC41IDkwLjUtMzcuNSA5MC41IDM3LjUgMzcuNSA5MC41em0xMjAyIDQ5OHEwIDUyLTM4IDkwdC05MCAzOHEtNTMgMC05MC41LTM3LjV0LTM3LjUtOTAuNSAzNy41LTkwLjUgOTAuNS0zNy41IDkwLjUgMzcuNSAzNy41IDkwLjV6bS05NjQtOTk2cTAgNjYtNDcgMTEzdC0xMTMgNDctMTEzLTQ3LTQ3LTExMyA0Ny0xMTMgMTEzLTQ3IDExMyA0NyA0NyAxMTN6bTExNzAgNDk4cTAgNTMtMzcuNSA5MC41dC05MC41IDM3LjUtOTAuNS0zNy41LTM3LjUtOTAuNSAzNy41LTkwLjUgOTAuNS0zNy41IDkwLjUgMzcuNSAzNy41IDkwLjV6bS02NDAtNzA0cTAgODAtNTYgMTM2dC0xMzYgNTYtMTM2LTU2LTU2LTEzNiA1Ni0xMzYgMTM2LTU2IDEzNiA1NiA1NiAxMzZ6bTUzMCAyMDZxMCA5My02NiAxNTguNXQtMTU4IDY1LjVxLTkzIDAtMTU4LjUtNjUuNXQtNjUuNS0xNTguNXEwLTkyIDY1LjUtMTU4dDE1OC41LTY2cTkyIDAgMTU4IDY2dDY2IDE1OHoiLz48L3N2Zz4=);animation:spin 2s infinite linear}@keyframes spin{from{box-sizing:border-box;transform:rotate(0deg)}to{box-sizing:border-box;transform:rotate(359.9deg)}}.gitter-chat-embed-action-bar{box-sizing:border-box;position:absolute;top:0;left:0;right:0;display:-ms-flexbox;display:flex;-ms-flex-pack:end;justify-content:flex-end;padding-bottom:.7em;background:linear-gradient(to bottom,#fff 0,#fff 50%,rgba(255,255,255,0) 100%)}.gitter-chat-embed-action-bar-item{box-sizing:border-box;display:-ms-flexbox;display:flex;-ms-flex-pack:center;justify-content:center;-ms-flex-align:center;align-items:center;width:40px;height:40px;padding-left:0;padding-right:0;opacity:.65;background:none;background-position:center center;background-repeat:no-repeat;background-size:22px 22px;border:0;outline:none;cursor:pointer;cursor:hand;transition:all .2s ease}.gitter-chat-embed-action-bar-item:hover,.gitter-chat-embed-action-bar-item:focus{box-sizing:border-box;opacity:1}.gitter-chat-embed-action-bar-item:active{box-sizing:border-box;filter:hue-rotate(80deg) saturate(150)}.gitter-chat-embed-action-bar-item-pop-out{box-sizing:border-box;margin-right:-4px;background-image:url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyMDAgMTcxLjQyOSIgZmlsbD0iIzNhMzEzMyI+PHBhdGggZD0iTTE1Ny4xNDMsMTAzLjU3MXYzNS43MTRjMCw4Ljg1NC0zLjE0NCwxNi40MjYtOS40MzEsMjIuNzEzcy0xMy44NTgsOS40MzEtMjIuNzEyLDkuNDMxSDMyLjE0MyBjLTguODU0LDAtMTYuNDI1LTMuMTQ0LTIyLjcxMi05LjQzMVMwLDE0OC4xNCwwLDEzOS4yODVWNDYuNDI5YzAtOC44NTQsMy4xNDQtMTYuNDI1LDkuNDMxLTIyLjcxMiBjNi4yODctNi4yODcsMTMuODU4LTkuNDMxLDIyLjcxMi05LjQzMWg3OC41NzJjMS4wNDEsMCwxLjg5NiwwLjMzNSwyLjU2NiwxLjAwNGMwLjY3LDAuNjcsMS4wMDQsMS41MjUsMS4wMDQsMi41NjdWMjUgYzAsMS4wNDItMC4zMzQsMS44OTctMS4wMDQsMi41NjdjLTAuNjcsMC42Ny0xLjUyNSwxLjAwNC0yLjU2NiwxLjAwNEgzMi4xNDNjLTQuOTExLDAtOS4xMTUsMS43NDktMTIuNjEyLDUuMjQ2IHMtNS4yNDYsNy43MDEtNS4yNDYsMTIuNjEydjkyLjg1NmMwLDQuOTExLDEuNzQ5LDkuMTE1LDUuMjQ2LDEyLjYxMnM3LjcwMSw1LjI0NSwxMi42MTIsNS4yNDVIMTI1YzQuOTEsMCw5LjExNS0xLjc0OCwxMi42MTEtNS4yNDUgYzMuNDk3LTMuNDk3LDUuMjQ2LTcuNzAxLDUuMjQ2LTEyLjYxMnYtMzUuNzE0YzAtMS4wNDIsMC4zMzQtMS44OTcsMS4wMDQtMi41NjdjMC42Ny0wLjY2OSwxLjUyNS0xLjAwNCwyLjU2Ny0xLjAwNGg3LjE0MyBjMS4wNDIsMCwxLjg5NywwLjMzNSwyLjU2NywxLjAwNEMxNTYuODA5LDEwMS42NzQsMTU3LjE0MywxMDIuNTI5LDE1Ny4xNDMsMTAzLjU3MXogTTIwMCw3LjE0M3Y1Ny4xNDMgYzAsMS45MzUtMC43MDcsMy42MDktMi4xMjEsNS4wMjJjLTEuNDEzLDEuNDE0LTMuMDg4LDIuMTIxLTUuMDIxLDIuMTIxYy0xLjkzNSwwLTMuNjA5LTAuNzA3LTUuMDIyLTIuMTIxbC0xOS42NDQtMTkuNjQzIGwtNzIuNzY3LDcyLjc2OWMtMC43NDQsMC43NDQtMS42LDEuMTE1LTIuNTY3LDEuMTE1cy0xLjgyMy0wLjM3MS0yLjU2Ny0xLjExNUw3Ny41NjcsMTA5LjcxYy0wLjc0NC0wLjc0NC0xLjExNi0xLjYtMS4xMTYtMi41NjcgYzAtMC45NjcsMC4zNzItMS44MjIsMS4xMTYtMi41NjZsNzIuNzY4LTcyLjc2OGwtMTkuNjQ0LTE5LjY0M2MtMS40MTMtMS40MTQtMi4xMi0zLjA4OC0yLjEyLTUuMDIyYzAtMS45MzUsMC43MDctMy42MDksMi4xMi01LjAyMiBDMTMyLjEwNSwwLjcwNywxMzMuNzc5LDAsMTM1LjcxNSwwaDU3LjE0M2MxLjkzNCwwLDMuNjA4LDAuNzA3LDUuMDIxLDIuMTIxQzE5OS4yOTMsMy41MzQsMjAwLDUuMjA4LDIwMCw3LjE0M3oiLz48L3N2Zz4=)}.gitter-chat-embed-action-bar-item-collapse-chat{box-sizing:border-box;background-image:url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAxNzEuNDI5IDE3MS40MjkiIGZpbGw9IiMzYTMxMzMiPjxwYXRoIGQ9Ik0xMjIuNDMzLDEwNi4xMzhsLTE2LjI5NSwxNi4yOTVjLTAuNzQ0LDAuNzQ0LTEuNiwxLjExNi0yLjU2NiwxLjExNmMtMC45NjgsMC0xLjgyMy0wLjM3Mi0yLjU2Ny0xLjExNmwtMTUuMjktMTUuMjkgbC0xNS4yOSwxNS4yOWMtMC43NDQsMC43NDQtMS42LDEuMTE2LTIuNTY3LDEuMTE2cy0xLjgyMy0wLjM3Mi0yLjU2Ny0xLjExNmwtMTYuMjk0LTE2LjI5NWMtMC43NDQtMC43NDQtMS4xMTYtMS42LTEuMTE2LTIuNTY2IGMwLTAuOTY4LDAuMzcyLTEuODIzLDEuMTE2LTIuNTY3bDE1LjI5LTE1LjI5bC0xNS4yOS0xNS4yOWMtMC43NDQtMC43NDQtMS4xMTYtMS42LTEuMTE2LTIuNTY3czAuMzcyLTEuODIzLDEuMTE2LTIuNTY3IEw2NS4yOSw0OC45OTZjMC43NDQtMC43NDQsMS42LTEuMTE2LDIuNTY3LTEuMTE2czEuODIzLDAuMzcyLDIuNTY3LDEuMTE2bDE1LjI5LDE1LjI5bDE1LjI5LTE1LjI5IGMwLjc0NC0wLjc0NCwxLjYtMS4xMTYsMi41NjctMS4xMTZjMC45NjcsMCwxLjgyMiwwLjM3MiwyLjU2NiwxLjExNmwxNi4yOTUsMTYuMjk0YzAuNzQ0LDAuNzQ0LDEuMTE2LDEuNiwxLjExNiwyLjU2NyBzLTAuMzcyLDEuODIzLTEuMTE2LDIuNTY3bC0xNS4yOSwxNS4yOWwxNS4yOSwxNS4yOWMwLjc0NCwwLjc0NCwxLjExNiwxLjYsMS4xMTYsMi41NjcgQzEyMy41NDksMTA0LjUzOSwxMjMuMTc3LDEwNS4zOTQsMTIyLjQzMywxMDYuMTM4eiBNMTQ2LjQyOSw4NS43MTRjMC0xMS4wMTItMi43MTctMjEuMTY4LTguMTQ4LTMwLjQ2OSBzLTEyLjc5Ny0xNi42NjctMjIuMDk4LTIyLjA5OFM5Ni43MjYsMjUsODUuNzE0LDI1cy0yMS4xNjgsMi43MTYtMzAuNDY5LDguMTQ3UzM4LjU3OSw0NS45NDUsMzMuMTQ3LDU1LjI0NlMyNSw3NC43MDMsMjUsODUuNzE0IHMyLjcxNiwyMS4xNjgsOC4xNDcsMzAuNDY5czEyLjc5NywxNi42NjYsMjIuMDk4LDIyLjA5OHMxOS40NTcsOC4xNDgsMzAuNDY5LDguMTQ4czIxLjE2OC0yLjcxNywzMC40NjktOC4xNDggczE2LjY2Ni0xMi43OTcsMjIuMDk4LTIyLjA5OFMxNDYuNDI5LDk2LjcyNiwxNDYuNDI5LDg1LjcxNHogTTE3MS40MjksODUuNzE0YzAsMTUuNTUxLTMuODMyLDI5Ljg5My0xMS40OTYsNDMuMDI0IGMtNy42NjQsMTMuMTMzLTE4LjA2MiwyMy41My0zMS4xOTQsMzEuMTk0Yy0xMy4xMzIsNy42NjQtMjcuNDc0LDExLjQ5Ni00My4wMjQsMTEuNDk2cy0yOS44OTItMy44MzItNDMuMDI0LTExLjQ5NiBjLTEzLjEzMy03LjY2NC0yMy41MzEtMTguMDYyLTMxLjE5NC0zMS4xOTRDMy44MzIsMTE1LjYwNywwLDEwMS4yNjUsMCw4NS43MTRTMy44MzIsNTUuODIyLDExLjQ5Niw0Mi42OSBjNy42NjQtMTMuMTMzLDE4LjA2Mi0yMy41MzEsMzEuMTk0LTMxLjE5NEM1NS44MjIsMy44MzIsNzAuMTY0LDAsODUuNzE0LDBzMjkuODkzLDMuODMyLDQzLjAyNCwxMS40OTYgYzEzLjEzMyw3LjY2NCwyMy41MywxOC4wNjIsMzEuMTk0LDMxLjE5NEMxNjcuNTk3LDU1LjgyMiwxNzEuNDI5LDcwLjE2NCwxNzEuNDI5LDg1LjcxNHoiLz48L3N2Zz4=)}.gitter-open-chat-button{box-sizing:border-box;z-index:100;position:fixed;bottom:0;right:10px;padding:1em 3em;background-color:#36bc98;border:0;border-top-left-radius:.5em;border-top-right-radius:.5em;color:#fff;font-family:sans-serif;font-size:12px;letter-spacing:1px;text-transform:uppercase;text-align:center;text-decoration:none;cursor:pointer;cursor:hand;transition:all .3s ease}.gitter-open-chat-button:visited{box-sizing:border-box;color:#fff}.gitter-open-chat-button:hover,.gitter-open-chat-button:focus{box-sizing:border-box;background-color:#3ea07f;color:#fff}.gitter-open-chat-button:focus{box-sizing:border-box;box-shadow:0 0 8px rgba(62,160,127,.6);outline:none}.gitter-open-chat-button:active{box-sizing:border-box;color:#eee}.gitter-open-chat-button.is-collapsed{box-sizing:border-box;transform:translateY(120%)}";
styleInject(css);

// DOM Utility
// https://gist.github.com/MadLittleMods/f71b0ef905832b8c16c9
//
// Inspired by bling.js: https://gist.github.com/paulirish/12fb951a8b893a454b32
// But we needed full module encapsulation
// This will concat anything including array-like things(like NodeLists or HTMLCollections)
var concat = function concat() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return args.reduce(function (result, item) {
    // If array-like
    if (item && item.length !== undefined && !Array.isArray(item) && ( // The window object acts as an array of the iframes in the document (undesired effects for our use cases)
    !window || window && !(item instanceof window.constructor))) {
      item = Array.prototype.slice.call(item);
    }

    return result.concat(item);
  }, []);
}; // Pass in a selector string, dom node, or array of dom nodes


function $() {
  for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    args[_key2] = arguments[_key2];
  }

  var elements = args;

  if (typeof args[0] === 'string') {
    var _document$querySelect;

    elements = (_document$querySelect = document.querySelectorAll).call.apply(_document$querySelect, [document].concat(args));
  }

  return concat.apply(null, elements);
}

function forEach(arrayLike, cb) {
  concat(arrayLike).forEach(function () {
    cb.apply(void 0, arguments);
  });
} // Listen to events.
// Pass in a string name of events separated by spaces

function on(elements, names, cb) {
  names.split(/\s/).forEach(function (name) {
    forEach(elements, function (element) {
      element.addEventListener(name, cb);
    });
  }); // Keep the chaining going

  return this;
} // Remove the event listener
// Pass in a string name of events separated by spaces

function off(elements, names, cb) {
  names.split(/\s/).forEach(function (name) {
    forEach(elements, function (element) {
      element.removeEventListener(name, cb);
    });
  }); // Keep the chaining going

  return this;
}
function prependElementTo(element, target) {
  var firstTargetChild = (target.children || [])[0];

  if (firstTargetChild) {
    target.insertBefore(element, firstTargetChild);
  } else {
    target.appendChild(element);
  }
} // Can't use `classList.toggle` with the second parameter (force)
// Because IE11 does not support it

function toggleClass(element, class1, force) {
  if (force !== undefined) {
    if (force) {
      element.classList.add(class1);
    } else {
      element.classList.remove(class1);
    }
  } else {
    element.classList.toggle(class1);
  }

  return force;
}

// Copy a JS object and make it read-only
function makeReadableCopy(obj) {
  var clone = {};
  Object.keys(obj).forEach(function (key) {
    Object.defineProperty(clone, key, {
      value: obj[key],
      writable: false,
      configurable: false
    });
  });
  return clone;
}

var parseAttributeTruthiness = function parseAttributeTruthiness(value) {
  if (value) {
    var valueSanitized = value.trim().toLowerCase();

    if (valueSanitized === 'true' || valueSanitized === '1') {
      return true;
    } else if (valueSanitized === 'false' || valueSanitized === '0') {
      return false;
    }
  }

  return value;
}; // Pass in a shape object of options and the element
// and we will extend and properties available
// NOTE: We will only look for keys present in `options` passed in


var getDataOptionsFromElement = function getDataOptionsFromElement(options, element) {
  if (!element) {
    return options;
  }

  var newOptions = {};
  Object.keys(options).forEach(function (optionKey) {
    var attr = "data-".concat(optionKey);

    if (element.hasAttribute(attr)) {
      newOptions[optionKey] = element.getAttribute(attr);
    } else {
      newOptions[optionKey] = options[optionKey];
    }
  });
  return newOptions;
}; // Helper method that detects whether an element was "activated"
// Returns a function that you can execute to remove the listeners
// Accibility in mind: click, spacebar, enter


var spacebarKey = 32;
var enterKey = 13;

var elementOnActivate = function elementOnActivate(elements, cb) {
  elements = $(elements);

  var handler = function handler(e) {
    // If click or spacebar, or enter is pressed
    if (e.type === 'click' || e.type === 'keydown' && (e.keyCode === spacebarKey || e.keyCode === enterKey)) {
      for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
        args[_key - 1] = arguments[_key];
      }

      cb.call.apply(cb, [this, e].concat(args));
    }
  };

  on(elements, 'click keydown', handler);
  return function () {
    off(elements, 'click keydown', handler);
  };
};

var embedGitterStyles = function embedGitterStyles() {
  var elementStore = new ElementStore(); //$('head')[0].insertAdjacentHTML('afterbegin', '<div></div>');

  var style = elementStore.createElement('style');
  style.innerHTML = css; // Put it at the top of the head so others can override

  prependElementTo(style, $('head')[0]);
  return elementStore;
};

var embedGitterChat = function embedGitterChat(opts) {
  var elementStore = new ElementStore();
  var targetElements = opts.targetElement;
  targetElements.forEach(function (targetElement) {
    var targetElementOpts = getDataOptionsFromElement(opts, targetElement);

    if (targetElementOpts.room) {
      var iframe = elementStore.createElement('iframe');
      iframe.setAttribute('frameborder', '0');
      iframe.src = "".concat(opts.host).concat(targetElementOpts.room, "/~embed"); //iframe.src = `${opts.host}${targetElementOpts.room}/~chat`;

      targetElement.appendChild(iframe);
    } else {
      console.error('Gitter Sidecar: No room specified for targetElement', targetElement);
    }
  });
  return elementStore;
}; // chat: sidecar chat instance


var addActionBar = function addActionBar(chat) {
  var opts = chat.options;
  var elementStore = new ElementStore();
  opts.targetElement.forEach(function (targetElement) {
    var actionBar = elementStore.createElement('div');
    actionBar.classList.add('gitter-chat-embed-action-bar'); // Prepend to the target

    targetElement.insertBefore(actionBar, targetElement.firstChild); // Add a couple buttons to the bar
    // ------------------------------------

    var popOutActionElement = elementStore.createElement('a'); // We don't combine the `classList` call because IE doesn't support it

    popOutActionElement.classList.add('gitter-chat-embed-action-bar-item');
    popOutActionElement.classList.add('gitter-chat-embed-action-bar-item-pop-out');
    popOutActionElement.setAttribute('aria-label', 'Open Chat in Gitter.im');
    popOutActionElement.setAttribute('href', "".concat(opts.host).concat(opts.room));
    popOutActionElement.setAttribute('target', "_blank");
    popOutActionElement.setAttribute('rel', "noopener");
    actionBar.appendChild(popOutActionElement);
    var collapseActionElement = elementStore.createElement('button'); // We don't combine the `classList` call because IE doesn't support it

    collapseActionElement.classList.add('gitter-chat-embed-action-bar-item');
    collapseActionElement.classList.add('gitter-chat-embed-action-bar-item-collapse-chat');
    collapseActionElement.setAttribute('aria-label', 'Collapse Gitter Chat');
    elementOnActivate(collapseActionElement, function (e) {
      // Hide the chat
      chat.toggleChat(false);
      e.preventDefault();
    });
    actionBar.appendChild(collapseActionElement);
  });
  return elementStore;
};

var documentRootElement = document.body || document.documentElement;
var defaults = {
  room: undefined,
  // Single or array of dom elements, or string selector to embed chat in
  // Where you want to embed the chat
  targetElement: undefined,
  // Single or array of dom elements, or string selector to embed chat in
  // The button element used to activate when the chat gets shown on the page
  // Note: Only applies if `options.showChatByDefault` is `false`
  activationElement: undefined,
  // Whether to show the chat embed when the page loads
  // Note: Use with caution, useful for use cases where you have a page dedicated to chat.
  showChatByDefault: false,
  // Whether to preload the gitter chat iframe.
  // We preload the chat so there isn't any jank when the chat opens
  preload: false,
  // Whether to embed a `<style>` tag with some pre-made CSS
  useStyles: true,
  // TODO: implement layouts (see todo.md)
  //   - `fixed`
  //   - `off-canvas`
  //   - `flex-aside`
  layout: 'fixed',
  //showLeftMenu: false
  // Undocumented private options ;)
  // Base URL of the gitter instance you are running
  // We are not using a nice URL parser/formatter,
  // so make sure to add the trailing slash so that concating goes smooth
  host: 'https://gitter.im/'
}; // Keep some stuff behind symbols so people "can't" access the private data

var DEFAULTS = BasicSymbol('DEFAULTS');

var OPTIONS = BasicSymbol('OPTIONS');

var ELEMENTSTORE = BasicSymbol('ELEMENTSTORE');

var EVENTHANDLESTORE = BasicSymbol('EVENTHANDLESTORE');

var INIT = BasicSymbol('INIT');

var ISEMBEDDED = BasicSymbol('ISEMBEDDED');

var EMBEDCHATONCE = BasicSymbol('EMBEDCHATONCE');

var TOGGLETARGETELEMENTS = BasicSymbol('TOGGLETARGETELEMENTS');

var chatEmbed =
/*#__PURE__*/
function () {
  function chatEmbed() {
    var _this = this;

    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, chatEmbed);

    this[ELEMENTSTORE] = new ElementStore();
    this[EVENTHANDLESTORE] = [];
    this[DEFAULTS] = {};
    this[OPTIONS] = {};
    Object.keys(defaults).forEach(function (key) {
      _this[DEFAULTS][key] = defaults[key];
      _this[OPTIONS][key] = defaults[key];
    });
    Object.keys(options).forEach(function (key) {
      _this[OPTIONS][key] = options[key];
    });
    this[INIT]();
  }

  _createClass(chatEmbed, [{
    key: INIT,
    value: function value() {
      var _this2 = this;

      var opts = this[OPTIONS];

      if (opts.useStyles) {
        this[ELEMENTSTORE].add(embedGitterStyles());
      } // Coerce into array of dom elements on what they pass in
      // Otherwise create our own default targetElement


      opts.targetElement = $(opts.targetElement || function () {
        var targetElement = _this2[ELEMENTSTORE].createElement('aside');

        targetElement.classList.add('gitter-chat-embed'); // Start out collapsed

        targetElement.classList.add('is-collapsed');
        documentRootElement.appendChild(targetElement);
        return targetElement;
      }());
      opts.targetElement.forEach(function (targetElement) {
        var loadingIndicatorElement = _this2[ELEMENTSTORE].createElement('div');

        loadingIndicatorElement.classList.add('gitter-chat-embed-loading-wrapper');
        loadingIndicatorElement.innerHTML = "\n        <div class=\"gitter-chat-embed-loading-indicator gitter-icon\"></div>\n      "; // Prepend

        targetElement.insertBefore(loadingIndicatorElement, targetElement.firstChild);
      }); // Add the action bar to the target
      // after it was put in place just above

      addActionBar(this);

      if (opts.preload) {
        this.toggleChat(false);
      }

      if (opts.showChatByDefault) {
        this.toggleChat(true);
      } // The activationElement is only setup if `opts.showChatByDefault` is false
      else {
          // Create our own default activationElement if one was not defined
          // Note: You can pass `false` or `null` to disable the activation element
          if (opts.activationElement === undefined || opts.activationElement === true) {
            opts.activationElement = $(function () {
              var button = _this2[ELEMENTSTORE].createElement('a'); // We use the option for the room (not pertaining to a particular targetElement attribute if set)


              button.href = "".concat(opts.host).concat(opts.room);
              button.innerHTML = 'Open Chat';
              button.classList.add('gitter-open-chat-button');
              documentRootElement.appendChild(button);
              return button;
            }());
          } // Otherwise coerce into array of dom elements on what they pass in
          else if (opts.activationElement) {
              opts.activationElement = $(opts.activationElement);
            }

          if (opts.activationElement) {
            // Hook up the button to show the chat on activation
            elementOnActivate(opts.activationElement, function (e) {
              // Show the chat
              _this2.toggleChat(true);

              e.preventDefault();
            }); // Toggle the visibility of the activation element
            // so it is only there when the the chat is closed

            opts.targetElement.forEach(function (targetElement) {
              on(targetElement, 'gitter-chat-toggle', function (e) {
                var isChatOpen = e.detail.state;
                opts.activationElement.forEach(function (activationElement) {
                  toggleClass(activationElement, 'is-collapsed', isChatOpen);
                });
              });
            });
          }
        } // Listen to buttons with a class of `.js-gitter-toggle-chat-button`
      // We also look for an options `data-gitter-toggle-chat-state` attribute


      var classToggleButtonOff = elementOnActivate($('.js-gitter-toggle-chat-button'), function (e) {
        var state = parseAttributeTruthiness(e.target.getAttribute('data-gitter-toggle-chat-state'));

        _this2.toggleChat(state !== null ? state : 'toggle');

        e.preventDefault();
      });
      this[EVENTHANDLESTORE].push(classToggleButtonOff); // Emit that we started on each targetElement

      opts.targetElement.forEach(function (targetElement) {
        var event = new CustomEvent('gitter-chat-started', {
          detail: {
            chat: _this2
          }
        });
        targetElement.dispatchEvent(event);
      }); // Emit that we started on the document

      var documentEvent = new CustomEvent('gitter-sidecar-instance-started', {
        detail: {
          chat: this
        }
      });
      document.dispatchEvent(documentEvent);
    }
  }, {
    key: EMBEDCHATONCE,
    value: function value() {
      if (!this[ISEMBEDDED]) {
        var opts = this[OPTIONS];
        var embedResult = embedGitterChat(opts);
        this[ELEMENTSTORE].add(embedResult);
      }

      this[ISEMBEDDED] = true;
    } // state: true, false, 'toggle'

  }, {
    key: TOGGLETARGETELEMENTS,
    value: function value(state) {
      var opts = this[OPTIONS];

      if (!opts.targetElement) {
        console.warn('Gitter Sidecar: No chat embed elements to toggle visibility on');
      }

      var targetElements = opts.targetElement;
      targetElements.forEach(function (targetElement) {
        if (state === 'toggle') {
          toggleClass(targetElement, 'is-collapsed');
        } else {
          toggleClass(targetElement, 'is-collapsed', !state);
        }

        var event = new CustomEvent('gitter-chat-toggle', {
          detail: {
            state: state
          }
        });
        targetElement.dispatchEvent(event);
      });
    } // Public API

  }, {
    key: "toggleChat",
    // state: true, false, 'toggle'
    value: function toggleChat(state) {
      var _this3 = this;

      var opts = this[OPTIONS]; // We delay the embed to make sure the animation can go jank free
      // if it isn't already embedded

      if (state && !this[ISEMBEDDED]) {
        var targetElements = opts.targetElement; // Start the loading spinner

        targetElements.forEach(function (targetElement) {
          targetElement.classList.add('is-loading');
        });
        setTimeout(function () {
          _this3[EMBEDCHATONCE]();

          _this3[TOGGLETARGETELEMENTS](state); // Remove the loading spinner


          targetElements.forEach(function (targetElement) {
            targetElement.classList.remove('is-loading');
          });
        }, 300
        /* TODO change to transition/animation end, see for robust transition/animation end code: https://github.com/MadLittleMods/jquery-carouselss */
        );
      } // But we still want people to embed no matter what state :)
      // For example `options.preload`, should load the chat but not show it
      else {
          this[EMBEDCHATONCE]();
          this[TOGGLETARGETELEMENTS](state);
        }
    }
  }, {
    key: "destroy",
    value: function destroy() {
      // Remove all the event handlers
      this[EVENTHANDLESTORE].forEach(function (fn) {
        fn();
      }); //console.log(this[ELEMENTSTORE]);
      // Remove and DOM elements, we made

      this[ELEMENTSTORE].destroy();
    }
  }, {
    key: "options",
    get: function get() {
      // We don't want anyone to modify our options
      // So copy and make it non-writable
      return makeReadableCopy(this[OPTIONS]);
    }
  }]);

  return chatEmbed;
}();

// This is the entry point for the <script> tag window global friendly version: https://sidecar.gitter.im/

var getOrDefaultKey = function getOrDefaultKey(obj, key) {
  return obj[key] || function () {
    obj[key] = {};
    return obj[key];
  }();
};

var windowGitter = getOrDefaultKey(window, 'gitter');
windowGitter.Chat = chatEmbed; // Tell them that `sidecar` is loaded and ready

var event = new CustomEvent('gitter-sidecar-ready', {
  detail: sidecar
});
document.dispatchEvent(event); // Create the default instance

if (!((windowGitter.chat || {}).options || {}).disableDefaultChat) {
  var windowGitterChat = getOrDefaultKey(windowGitter, 'chat');
  windowGitterChat.defaultChat = new chatEmbed(windowGitterChat.options || {});
}

module.exports = sidecar;
