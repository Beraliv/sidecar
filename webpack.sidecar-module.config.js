var path = require('path');
var webpackConfig = require('./webpack.config');

module.exports = Object.assign({}, webpackConfig, {
  entry: './src/index.module.js',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'sidecar-module.js',
    library: 'sidecar',
    libraryTarget: 'umd'
  }
});
