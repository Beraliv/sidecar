var path = require('path');
var webpack = require('webpack');

var manifest = require('./package.json');

var commentHeader = 'Gitter Sidecar v' + manifest.version + '\nhttps://sidecar.gitter.im/';

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'sidecar.js',
    library: 'sidecar'
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|bower_components)/,
        loader: 'babel-loader'
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'css-loader', options: { importLoaders: 1 } },
          'postcss-loader',
        ]
      }
    ]
  },
  plugins: [
    new webpack.BannerPlugin(commentHeader)
  ],
};
