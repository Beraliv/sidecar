import rollupConfig from './rollup.config';

export default Object.assign({}, rollupConfig, {
  input: './src/index.module.js',
  output: {
    file: './dist/sidecar-module.rollup.js',
    format: 'umd',
    library: 'sidecar',
  }
});